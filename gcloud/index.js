// Imports the Google Cloud client library
const Datastore = require('@google-cloud/datastore');

// Creates a client
const datastore = new Datastore({
	projectId: 'fsd-gcloud-project',
	keyFilename: 'file.json'
});
const kindName = 'task'


async function saveTask(text){

	const taskKey = datastore.key(kindName);
	const task = {
		key : taskKey,
		data : {
			description: text,
		},
	};
	await datastore.save(task);
}

async function listTasks() {
	const query = datastore.createQuery('task').order('description');
	const [tasks] = await datastore.runQuery(query);
	console.log('Tasks:');
	for (const task of tasks) {
	  const taskKey = task[datastore.KEY];
	  console.log(taskKey.id, task);
	}
}

exports.addTask = (req,res) => {
	let task = req.query.description || req.body.description || '';
	saveTask(task);
	res.send(task);
}
exports.getTasks = (req,res) => {
	listTasks();
}
