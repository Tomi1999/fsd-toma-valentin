import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";
import * as jwt from "jsonwebtoken";
import { env } from "../env";

export class UserController {

    private userRepository = getRepository(User);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.userRepository.findOne(request.params.id);
        response.statusMessage = "Deleted";
        await this.userRepository.remove(userToRemove);
    }
    async login(request: Request, response: Response, next: NextFunction){

        let { email, password } = request.body;
        let user : User;
        user = await this.userRepository.findOne({where: { email,password }});
        if(user) {
        const token = jwt.sign({ userId: user.id, email: user.email }, env.token , {expiresIn: "1h"});
        response.statusMessage = "Valid credentials"
        response.statusCode = 200
        response.json({token : token})
        } else {
            response.statusMessage = "Invalid credentials"
            response.sendStatus(401);
        }
    };
    async register(request: Request, response: Response, next: NextFunction){
        if(await this.userRepository.findOne({email: request.body.email}) != null) {
            response.statusMessage = "An account has already been registered using this e-mail address"
            return response.sendStatus(403)
        }
        const user = new User
        user.email = request.body.email;
        user.password = request.body.password;
        try {
            await this.userRepository.save(user)
            response.statusMessage = "User registered"
            response.statusCode = 200
            return response.send()
        }
        catch {
            response.statusMessage = "Error while registering"
            return response.sendStatus(500)
        }

    }

}