import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Note} from "../entity/Note";


export class NoteController {

    private noteRepository = getRepository(Note);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.noteRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.noteRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.noteRepository.save(request.body);
    }

    async update(request: Request, response: Response, next: NextFunction) {
        let noteToEdit = await this.noteRepository.findOne(request.params.id);
        noteToEdit.title = request.body.title;
        noteToEdit.message = request.body.message;
        await this.noteRepository.update(request.params.id, noteToEdit);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let noteToRemove = await this.noteRepository.findOne(request.params.id);
        await this.noteRepository.remove(noteToRemove);
    }

}