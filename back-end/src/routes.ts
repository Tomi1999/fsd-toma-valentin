import { CommentController } from "./controller/CommentController";
import { NoteController } from "./controller/NoteController";
import {UserController} from "./controller/UserController";

export const Routes = [{
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all",
    guard: true
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one",
    guard: true
}, {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save",
    guard: true
}, {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove",
    guard: true
}, {
    method: "get",
    route: "/note",
    controller: NoteController,
    action: "all",
    guard: true
}, {
    method: "get",
    route: "/note/:id",
    controller: NoteController,
    action: "one",
    guard: true
}, {
    method: "post",
    route: "/note",
    controller: NoteController,
    action: "save",
    guard: true
}, {
    method: "delete",
    route: "/note/:id",
    controller: NoteController,
    action: "remove",
    guard: true
}, {
    method: "put",
    route: "/note/:id",
    controller: NoteController,
    action: "update",
    guard: true
}, {
    method: "get",
    route: "/comment",
    controller: CommentController,
    action: "all",
    guard: true
}, {
    method: "get",
    route: "/comment/:id",
    controller: CommentController,
    action: "one",
    guard: true
}, {
    method: "post",
    route: "/comment",
    controller: CommentController,
    action: "save",
    guard: true
}, {
    method: "delete",
    route: "/comment/:id",
    controller: CommentController,
    action: "remove",
    guard: true
}, {
    method: "post",
    route: "/register",
    controller: UserController,
    action: "register",
    guard: false
}, {
    method: "post",
    route: "/login",
    controller: UserController,
    action: "login",
    guard: false
}];