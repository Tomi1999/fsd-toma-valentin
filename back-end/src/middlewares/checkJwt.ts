import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import { env } from "../env";

export const checkJwt = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = jwt.verify(authHeader.split(' ')[1], env.token)
        if(token)
            return true;
        else {
            res.sendStatus(401);
            return false;
        }
    } else {
        res.sendStatus(401);
        return false;
    }
};