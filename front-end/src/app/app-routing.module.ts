import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JwtModule, JwtModuleOptions } from '@auth0/angular-jwt';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { NoteComponent } from './components/note/note.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuardGuard } from './guards/auth-guard.guard';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: "register", component: RegisterComponent},
  { path: "home", component: HomeComponent, canActivate: [AuthGuardGuard] },
  { path: "note", component: NoteComponent, canActivate: [AuthGuardGuard] },
  { path: '**', component: NotFoundComponent, canActivate: [AuthGuardGuard] },
];

export function tokenGetter() {
  return localStorage.getItem("access_token");
}
const JWT_Module_Options: JwtModuleOptions = {
  config: {
      tokenGetter: tokenGetter,
      allowedDomains: ["localhost:4200"]
  }
};

@NgModule({
  imports: [RouterModule.forRoot(routes), JwtModule.forRoot(JWT_Module_Options)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
