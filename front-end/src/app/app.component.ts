import { Component } from '@angular/core';
import { HeaderStateService } from './services/header-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public headerState : HeaderStateService) {
    this.headerState.headerEvent.subscribe((visible : boolean) => this.headerState.visible = visible);
   }

  title = 'front-end';
}
