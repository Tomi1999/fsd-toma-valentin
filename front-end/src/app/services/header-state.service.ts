import { EventEmitter, Injectable, Output } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderStateService {

  visible: boolean = true;
  @Output() headerEvent: EventEmitter<any> = new EventEmitter();
  constructor() { }

  show() {
    this.headerEvent.emit(true);
  }
  hide() {
    this.headerEvent.emit(false);
  }

}
