import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { User } from '../models/user';
import { RestRequestService } from './rest-request.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public restRequestService: RestRequestService<User>, public jwtHelper: JwtHelperService, public tokenService: TokenService) { }

  login(email: string, password: string) {
    return this.restRequestService.post({email, password}, "/api/login");
  }

  register(email: string, password: string) {
    return this.restRequestService.post({email, password}, "/api/register");
  }

  public isAuthenticated() : boolean {
    return !this.jwtHelper.isTokenExpired(this.tokenService.getToken().toString()); 
  }

}
