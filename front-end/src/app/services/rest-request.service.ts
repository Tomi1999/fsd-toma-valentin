import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { apiConfig } from '../config/api.config';


@Injectable({
  providedIn: 'root'
})
export class RestRequestService<T> {

  constructor(private http: HttpClient, public config: apiConfig) { }


  post(object : T, urlOption : string) : Observable<any> {
    return this.http.post<T>(this.config.rootUrl + urlOption, object);
  }

  get(urlOption : string) : Observable<any> {
    return this.http.get(this.config.rootUrl + urlOption, {responseType: 'text'});
  }

  put(object : T, urlOption : string) : Observable<any> {
    return this.http.put(this.config.rootUrl + urlOption, object);
  }

  delete(urlOption : string) {
    return this.http.delete(this.config.rootUrl + urlOption);
  }

}
