import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() {}

  public setToken(token:string ){
    localStorage.setItem('token', token);
  } 
  
  public getToken() : string {
    var token = localStorage.getItem('token');
    if(token) {
      return token;
    }
    return '';
  }

  public removeToken() {
    localStorage.setItem('token' , '');
  }

}
