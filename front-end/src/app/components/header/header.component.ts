import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderStateService } from 'src/app/services/header-state.service';
import { RestRequestService } from 'src/app/services/rest-request.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(public headerState : HeaderStateService, public tokenService: TokenService,private router : Router) { }

  ngOnInit(): void {
    
  }

  clicked(){
    this.tokenService.removeToken();
    this.router.navigate(['']);
  }

}
