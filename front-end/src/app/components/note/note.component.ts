import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Note } from 'src/app/models/note';
import { RestRequestService } from 'src/app/services/rest-request.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {

  public notes : Note[];
  noteForm = new FormGroup({
    title: new FormControl('',[Validators.required,Validators.minLength(1)]),
    message: new FormControl('',[Validators.required, Validators.minLength(1)]),
  });
  constructor(public restRequestService: RestRequestService<Note>,public dialog: MatDialog) { }

  ngOnInit(): void {
    this.showNotes();
  }

  showNotes(){
    this.restRequestService.get("/api/note").subscribe(note => {
      this.notes = JSON.parse(note) as Note[];
      this.notes.sort(function(a, b){return a.id - b.id})
    })
  }

  onSubmit(){
    if(this.noteForm.valid){
    const note = new Note();
    note.message = this.noteForm.get('message')?.value;
    note.title = this.noteForm.get('title')?.value
    this.restRequestService.post(note, "/api/note").subscribe(() => {
      this.noteForm.get('message')?.setValue("");
      this.noteForm.get('title')?.setValue("");
      this.showNotes();
    });}
  }

  async deleteNote(note : Note){
    await this.restRequestService.delete("/api/note/" + note.id).subscribe();
    this.showNotes();
  }

  editNote(note : Note) {
    this.dialog.open(NoteDialog, {
      data: {id : note.id, title : note.title, message : note.message}
    }).afterClosed().subscribe(() => this.showNotes());
  }

}

@Component({
  selector: 'note.dialog',
  templateUrl: 'note.dialog.html',
  styleUrls: ['./note.component.scss']
})
export class NoteDialog {

  noteForm = new FormGroup({
    title: new FormControl('',[Validators.required,Validators.minLength(1)]),
    message: new FormControl('',[Validators.required, Validators.minLength(1)]),
  });
  constructor(@Inject(MAT_DIALOG_DATA) public data: Note,private restRequestService : RestRequestService<Note>,public dialogRef: MatDialogRef<NoteDialog>) {
    this.noteForm.get('message')?.setValue(data.message);
    this.noteForm.get('title')?.setValue(data.title);
  }

  cancel(){
    this.dialogRef.close();
  }

  async onSubmit(){
    if(this.noteForm.valid){
    const note = new Note();
    note.message = this.noteForm.get('message')?.value;
    note.title = this.noteForm.get('title')?.value
    await this.restRequestService.put(note, "/api/note/" + this.data.id).subscribe();
    this.dialogRef.close();
    }
  }
}
