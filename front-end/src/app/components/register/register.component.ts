import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { HeaderStateService } from 'src/app/services/header-state.service';
import { TokenService } from 'src/app/services/token.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  profileForm = new FormGroup({
    email: new FormControl('',[Validators.required,Validators.pattern("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$")]),
    password: new FormControl('',[Validators.required, Validators.minLength(1)]),
  });
  url = "../../../assets/Tom.png";
  constructor(public headerState : HeaderStateService, private userSerivce : UserService, private router : Router) {
    this.headerState.hide();
   }
  onSubmit() {
    if(!this.profileForm.valid) {
      this.url="../../../assets/Tom2.png";
    }
    this.userSerivce.register(this.profileForm.get('email')?.value, this.profileForm.get('password')?.value).pipe;
    this.router.navigate(['home']);
  }
  ngOnInit(): void {
    
  }


}
