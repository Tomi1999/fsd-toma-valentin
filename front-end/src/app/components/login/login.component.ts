import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { HeaderStateService } from 'src/app/services/header-state.service';
import { TokenService } from 'src/app/services/token.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  profileForm = new FormGroup({
    email: new FormControl('',[Validators.required,Validators.pattern("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$")]),
    password: new FormControl('',[Validators.required, Validators.minLength(1)]),
  });
  url = "../../../assets/Tom.png";
  constructor(public headerState : HeaderStateService, private userSerivce : UserService, private tokenService : TokenService, private router : Router) {
    this.headerState.hide();
   }
  onSubmit() {
    if(!this.profileForm.valid) {
      this.url="../../../assets/Tom2.png";
    }
    this.userSerivce.login(this.profileForm.get('email')?.value, this.profileForm.get('password')?.value).subscribe(data => {

      if(data) {
        this.tokenService.setToken(data.token);
        this.router.navigate(['home']);
        this.headerState.show();
      }
    },err => {
      this.url="../../../assets/Tom2.png";
    })
  }
  ngOnInit(): void {
    
  }


}
